package main

import (
	"fmt"
	"math"
)

const pi = 3.14

func main() {
	var area float64

	fmt.Println("Введите площадь прямоугольника: ")
	fmt.Scan(&area)

	len := math.Sqrt(area * 4 * pi)
	diameter := len / pi
	fmt.Printf("Длина окружности: %5.2f\nДиаметр: %5.2f", len, diameter)
}
