package main

import (
	"fmt"
)

func main() {
	var side1, side2 int

	fmt.Println("Введите длину сторон прямоугольника через пробел: ")
	fmt.Scan(&side1, &side2)

	res := side1 * side2
	fmt.Printf("Площадь прямоугольника: %d", res)
}
