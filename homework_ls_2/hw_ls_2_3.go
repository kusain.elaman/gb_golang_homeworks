package main

import (
	"fmt"
)

func main() {
	var userNum float64

	fmt.Println("Введите трехзначное число: ")
	fmt.Scan(&userNum)

	if userNum < 100 || userNum > 999 {
		fmt.Println("Ошибка. Нужно ввести трехзначное число!")
	} else {
		hundreds := int(userNum / 100)
		tenners := int(userNum/10) % 10
		units := int(userNum) % 10

		fmt.Printf("Сотни:   %d\nДесятки: %d\nЕдиницы: %d\n", hundreds, tenners, units)

	}
}
