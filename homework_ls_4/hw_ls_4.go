package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := bufio.NewReader(os.Stdin)
	fmt.Println("Введите целые числа через пробел")
	line, err := r.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	ints, err := parseInts(line)
	if err == nil {
		ints = sort(ints)
		fmt.Println("Отсортированные числа:")
		fmt.Println(ints)
	}
}

func parseInts(s string) ([]int, error) {
	var (
		fields = strings.Fields(s)
		ints   = make([]int, len(fields))
		err    error
	)

	for i, f := range fields {
		ints[i], err = strconv.Atoi(f)
		if err != nil {
			fmt.Printf("%q не целое число", f)
			return nil, err
		}
	}
	return ints, nil
}

func sort(ints []int) []int {
	var n = len(ints)
	for i := 1; i < n; i++ {
		j := i
		for j > 0 {
			if ints[j-1] > ints[j] {
				ints[j-1], ints[j] = ints[j], ints[j-1]
			}
			j = j - 1
		}
	}
	return ints
}
