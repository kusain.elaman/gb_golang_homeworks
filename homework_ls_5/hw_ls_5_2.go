package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var results = map[int]int{1: 0, 2: 1}

func main() {
	for {
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Println("\t")
		fmt.Println("Введите порядковый номер. Это должно быть одно положительное целое число.")
		fmt.Println("Или введите 'stop', чтобы остановить работу прогарммы: ")
		scanner.Scan()

		scanNumber := scanner.Text()

		if strings.ToLower(scanNumber) == "stop" {
			fmt.Println("Goodbye")
			break
		}

		number, err := strconv.Atoi(scanNumber)
		if err != nil {
			fmt.Println("Вы ввели не целое число:")
		} else if number <= 0 {
			fmt.Println("Вы ввели ноль или число меньше нуля:")
		} else {
			if _, ok := results[number]; ok {
				fmt.Println("Ответ из мапа")
				fmt.Println(results[number])
			} else {
				fib(number)
				fmt.Println("В мапу добавлены новые данные")
				fmt.Println(results[number])
			}
		}

	}
}

func fib(number int) int {

	if number == 1 {
		return 0
	}
	if number == 2 {
		return 1
	}
	res := fib(number-1) + fib(number-2)
	mapAppend(number, res)
	return res
}

func mapAppend(number int, res int) {
	results[number] = res
}
