package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите порядковый номер. Это должно быть одно положительное целое число:")
	scanner.Scan()
	scanNumber := scanner.Text()
	number, err := strconv.Atoi(scanNumber)
	if err != nil {
		fmt.Println("Вы ввели не целое число:")
	} else if number <= 0 {
		fmt.Println("Вы ввели ноль или число меньше нуля:")
	} else {
		fmt.Println(fib(number))
	}
}

func fib(number int) int {

	if number == 1 {
		return 0
	}
	if number == 2 {
		return 1
	}

	return fib(number-1) + fib(number-2)
}
