package main

import (
	"flag"
	"fmt"
)

var (
	userNum = flag.Int("userNum", 0, "Users number")
)

func main() {
	flag.Parse()

	var num = *userNum
	/*
		if isPrime(num) {
			fmt.Printf("this num is prime")
		} else {
			fmt.Printf("this num is not prime")
		}
	*/

	for i := 0; i <= num; i++ {
		if isPrime(i) {
			fmt.Println(i)
		}
	}
}

func isPrime(number int) bool {
	if number == 1 || number <= 0 {
		return false
	} else {
		for i := 2; i*i <= number; i++ {
			if number%i == 0 {
				return false
			}
		}
		return true
	}
}
