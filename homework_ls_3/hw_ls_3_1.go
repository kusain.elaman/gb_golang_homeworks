package main

import (
	"flag"
	"fmt"
	"math"
)

const (
	OperationPlus  = "plus"
	OperationMinus = "minus"
	OperationDiv   = "divide"
	OperationMul   = "multiply"
	OperationSqrt  = "sqrt"
	OperationMod   = "mod"
	OperationPer   = "percent"
)

var (
	operand1Ptr = flag.Float64("operand1", 0, "Operand 1")
	operand2Ptr = flag.Float64("operand2", 0, "Operand 2")
	opPtr       = flag.String("op", "plus", "Operation for perform")

	AllOperations = []string{OperationPlus, OperationMinus, OperationDiv, OperationMul, OperationSqrt, OperationMod, OperationPer}
)

func main() {
	flag.Parse()

	var operand1 = *operand1Ptr
	var operand2 = *operand2Ptr
	var op = *opPtr

	var result float64

	switch op {
	case OperationPlus:
		result = operand1 + operand2
	case OperationMinus:
		result = operand1 - operand2
	case OperationMul:
		result = operand1 * operand2
	case OperationDiv:
		result = operand1 / operand2
	case OperationSqrt:
		result = math.Sqrt(operand1)
	case OperationMod:
		result = float64(int(operand1) % int(operand2))
	case OperationPer:
		result = operand1 * operand2 / 100
	default:
		fmt.Println("I don't know this operation.")
	}

	if math.IsInf(result, 0) {
		fmt.Println("You can't divide by zero.")
	} else if search(op) {
		fmt.Println("You can use next opetations:")
		for _, v := range AllOperations {
			fmt.Println(" * " + v)
		}
	} else {
		fmt.Printf("Result = %5.2f\n", result)
	}
}

func search(op string) bool {
	for _, v := range AllOperations {
		if v == op {
			return false
		}
	}
	return true
}
